/**
 * by oybcf
 * 2017-04-22 09:43
 * */
(function($) {
	$.fn.wapToast = function(options) {
		var defaultOp = {
			align: 'center',
			size: '12px',
			bottom: '60%',
			txt: 'hello, world!'
		}
		var toastSta;
		var dtd = $.extend(defaultOp, options);
		var toastDom = new Array();
		toastDom.push('<div id = "wapToast" style="position:fixed;left:10%;padding:8px;width:80%;color: #fff;background: rgba(0, 0, 0, .75);line-height:1.8;z-index:100;bottom:' + dtd.bottom + ';font-size:' + dtd.size + ';text-align:' + dtd.align + ';">' );
		toastDom.push('<p>' + dtd.txt + '</p>');
		toastDom.push('</div>');
		
		toastDom = toastDom.join('');
		if (!$("#wapToast").is(':visible')) {	// 防止多次点击
			$('body').append(toastDom);
		}

		// 显示
		$('#wapToast').fadeIn();
		setTimeout(function(){
			$('#wapToast').fadeOut();
		},2500)
		setTimeout(function(){
			$('#wapToast').remove();
		},3000)
	}
})(jQuery);