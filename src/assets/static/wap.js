$(function(){
	// 校验手机号
	function checkMobile(mobile){
		var reg = /^1[3-9]\d{9}$/;
		if (reg.test(mobile)) {
			return true;
		}else{
			return false;
		}
	}
	// 关闭modal
	function modalClose(){
		$('.modal').fadeOut();
		$('.modal_bg').hide();
	}
	// 成功modal
	function modalSuc(txt){
		$('.modalSuc').find('.summary').text(txt);
		$('.modal_bg').show();
		$('.modalSuc').fadeIn();
	}
	// 失败modal
	function modalFal(txt){
		$('.modalFal').find('.summary').text(txt);
		$('.modal_bg').show();
		$('.modalFal').fadeIn();
	}
	//
	function codeFun(){
		var t = 60;
		$('#btnCode').addClass('disabled');
		var set = setInterval(function(){
			t -= 1;
			if (t==0) {
				clearInterval(set);
				t = 60;
				$('#btnCode').text('获取');
				$('#btnCode').removeClass('disabled');
			}else{
				$('#btnCode').text(t);
			}
		}, 1000);
	}
	
	$('.modal_bg').tap(function(){
		modalClose();
	})
	$('.btn_close').tap(function(){
		modalClose();
	})
	
	// 老用户注册
	$('#register').tap(function(){
		var name = $('#name').val(),
			mobile = $('#mobile').val(),
			code = $('code').val();
		if (name == '') {
			$('body').wapToast({
				txt: '请输入您的姓名'
			})
		}else if(!checkMobile(mobile)){
			$('body').wapToast({
				txt: '请输入正确的手机号码'
			})
		}else if(code==''){
			$('body').wapToast({
				txt: '验证码错误'
			})
		}else{
			modalSuc('恭喜您，成为老用户！')
		}
	});
	
	// 我要推荐
	$('#btnRefree').tap(function(){
		var name = $('#name').val(),
			mobile = $('#mobile').val(),
			city = $('#city option:selected').val();
			address = $('#address').val(),
			code = $('code').val();
		if (name == '') {
			$('body').wapToast({
				txt: '请输入被推荐人姓名'
			})
		}else if(!checkMobile(mobile)){
			$('body').wapToast({
				txt: '请输入正确的手机号码'
			})
		}else if(city==''){
			$('body').wapToast({
				txt: '请选择所在地区'
			})
		}else if(address.length<5){
			$('body').wapToast({
				txt: '请填写被推荐人小区地址'
			})
		}else if(code==''){
			$('body').wapToast({
				txt: '验证码错误'
			})
		}else{
			modalSuc('提交成功！请等待审核')
		}
	});
	
	// 短信验证码
	$('#btnCode').tap(function(){
		var mobile = $('#mobile').val();
		if (checkMobile(mobile)) {
			if (!$('#btnCode').hasClass('disabled')) {
				codeFun();
			}
		}else{
			$('body').wapToast({
				txt: '请输入正确的手机号码'
			})
		}
	});

	// 个人中心跳转
	$('.ind_tabs li').tap(function(){
		var url = $(this).data('url');
		// 判断如果已注册跳相应地址，否则去注册
		if (1==2) {
			window.location.href = url;
		}else{
			window.location.href = 'register.html';
		}
	});
});
