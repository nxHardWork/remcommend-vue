import Vue from 'vue'
import Vuex from 'vuex'

import loginStroe from './loginStroe'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        slogan:'',
        historyLength:0
    },
    mutations:{
        /*
         * @desc 记录路由切换次数
         * @arg {object} state 状态
         */
        updateHistoryLength (state) {
            state.historyLength++
        }
    },
    modules:{
        loginStroe
    }
})
