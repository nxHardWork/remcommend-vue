import axios from 'axios'
import { baseUrl } from '../conf/env'
export default {
    state:{
        loginState:true,
        loginUrl:baseUrl + '/userIndex/vueIndex'
    },
    mutations:{
        initLoginState (state) {
        },
        loginFail (state) {
            console.log('登陆失败')
            state.loginState = false
        }
    },
    actions:{
        checkLoginState ({state, commit}, paramData) {
           // var $router = this.$router 这里是无法获得到$router 对象的
            console.log(paramData)
            let $router = paramData.$router
            axios({
                method:'get',
                url:paramData.url
            }).then((response) => {
                debugger
                let data = response.data
                if (data.state === 0) {
                    $router.push('/register')
                } else {
                    console.log('登陆成功')
                    $router.push('/index')
                }
            }).catch(function (error) {
                console.log(error)
            })
        }
    }
}
