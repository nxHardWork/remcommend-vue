import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import register from '@/components/register'
import referee from '@/components/referee'
import recommend from '@/components/recommend'
import reward from '@/components/reward'
import article from '@/components/article'
import login from '@/components/login'
import notFound from '@/components/notFound'

Vue.use(Router)

export default new Router({
    routes:[{
        path:'/login',
        name:'login',
        component:login
    }, {
        path:'/index',
        name:index,
        component:index
    }, {
        path:'/register',
        name:'register',
        component:register
    }, {
        path:'/referee',
        name:'referee',
        component:referee
    }, {
        path:'/recommend',
        name:'recommend',
        component:recommend
    }, {
        path:'/reward',
        name:'reward',
        component:reward
    }, {
        path:'/article',
        name:'article',
        component:article
    }, {
        path:'/*',
        name:'notFound',
        component:notFound
    }]
})
